import React, { useState } from 'react'
import { useSelector } from 'react-redux'

import RepoInfo from '../../components/UI/Card/Card'
import './Repository.css'
import axios from 'axios'

import { STAR_TOGGLER, WATCH_TOGGLER } from '../../query'

const Repository = ({
  viewerHasStarred,
  viewerSubscription,
  repository
}) => {
  const [viewerStarred, setViewerStarred] = useState(viewerHasStarred)
  const [viewerSubscriptionState, setViewerSubscription] = useState(viewerSubscription)
  const [starCount, setStarCount] = useState(repository.stargazers.totalCount)
  const [watchCount, setWatchCount] = useState(repository.watchers.totalCount)

  const token = useSelector(state => state.tokenValidator.token)

  const starToogler = () => {
    const starState = viewerStarred
    const repositoryId = repository.id
    let mutuateStar = STAR_TOGGLER()
    if (starState) { mutuateStar = mutuateStar.replace('addStar', 'removeStar') }
    axios.post('https://api.github.com/graphql', {
      query: mutuateStar,
      variables: { repositoryId }
    }, {
      headers: {
        Authorization: `bearer ${token}`
      }
    })
      .then(() => {
        let count = starCount
        if (starState) { count -= 1 } else count += 1

        setViewerStarred(!starState);
        setStarCount(count);
      })
      .catch((err) => {
        console.log(err)
      })
  }

  const watchToggler = () => {
    let subscribeState = viewerSubscriptionState
    subscribeState = subscribeState === 'SUBSCRIBED' ? 'UNSUBSCRIBED' : 'SUBSCRIBED'
    const repositoryId = repository.id
    const mutuateWatch = WATCH_TOGGLER
    axios.post('https://api.github.com/graphql', {
      query: mutuateWatch,
      variables: { repositoryId, subscribeState }
    }, {
      headers: {
        Authorization: `bearer ${token}`
      }
    })
      .then(() => {
        let count = this.state.watchCount
        count = subscribeState === 'SUBSCRIBED' ? count + 1 : count - 1
        setViewerSubscription(subscribeState);
        setWatchCount(count)
      })
      .catch((err) => {
        console.log(err)
      })
  }

  return (<div className="RepoCard">
       <RepoInfo
        repository={repository}
        viewerHasStarred={viewerStarred}
        viewerSubscription={viewerSubscriptionState}
        starToogler={starToogler}
        watchToggler={watchToggler}
        starCount={starCount}
        watchCount={watchCount}
       />
     </div>)
}

export default Repository;
