import React, { useState } from 'react'
import { Button } from '@material-ui/core'
import { useDispatch, useSelector } from 'react-redux'

import { withStyles } from '@material-ui/core/styles'

import * as actions from '../../store/actions/index'
import Repository from '../Repository/Repository'
import './RepoSearch.css'
import { GET_ORGANIZATION } from '../../query'

const useStyles = () => ({
  btnList: {
    fontSize: '2rem'
  }
})

const RepoSearch = (props) => {
  const [repoName, setRepoName] = useState('')
  const dispatch = useDispatch()

  const repo = useSelector(state => state.repoReducer.repo)
  const loading = useSelector(state => state.repoReducer.loading)
  const error = useSelector(state => state.repoReducer.error)
  const token = useSelector(state => state.tokenValidator.token)
  const pageInfo = useSelector(state => state.repoReducer.pageInfo)
  const status = useSelector(state => state.repoReducer.status)
  const repoCount = useSelector(state => state.repoReducer.repositoryCount)

  const repoSearch = (query, token) => dispatch(actions.repoSearch(query, token))
  const clearSearch = () => dispatch(actions.clearSearch())

  const handleForm = (e) => {
    e.preventDefault()
    clearSearch()
    const queryString = `is:public ${repoName} in:name`
    repoSearch(GET_ORGANIZATION(queryString), token)
  }

  const inputChangeHandler = (e) => setRepoName(e.target.value)

  const fetchMoreRepo = () => {
    const { endCursor } = pageInfo
    console.log(endCursor)
    const queryString = `is:public ${repoName} in:name`
    return repoSearch(GET_ORGANIZATION(queryString, endCursor), token)
  }
  const repositoryMapper = () => {
    const { classes } = props
    const list = repo.map(({ node }) =>
      (
        <Repository key = { node.id }
            repository = { node }
            viewerHasStarred = { node.viewerHasStarred }
            viewerSubscription = { node.viewerSubscription }
        />
      )
    )

    const moreList = pageInfo.hasNextPage
      ? <Button onClick = { fetchMoreRepo } className={classes.btnList}> More List </Button>
      : <Button href="#top" className={classes.btnList}>No more results,Go To Top</Button >

    return (
        <> { list } { moreList } </>
    )
  }

  const formElement = (
    <div className="repoBox">
        <form autoComplete = "off" onSubmit = {handleForm} className="formElement2">
            <input name = "repoName"
            onChange = {inputChangeHandler}
            className = "formInput"
            placeholder="Search Your Repository here..."
            value = {repoName}
            />

            <button color = "primary"
            className = "btn btn-search"
            type = "submit" > Search </button>
        </form>
        <button className = "btn btn-clear" onClick = {clearSearch}> Clear All Results </button>
    </div>
  )

  const repoList = repo.length ? repositoryMapper() : null
  const repoStatus = (<h4 className="status"> {status} </h4>)
  const repoCountElm = repoCount ? (<p className="searchCount"> Search Result: Total {repoCount} Repository </p>) : null

  return (
    <main className="RepoPage" id="top">
        {error 
          ? (<p> {error.message} </p>)
          :
          <>
            {formElement}
            {repoCountElm}
            {repoList}
            {repoStatus}
          </>
        }
    </main>
  )
}

export default withStyles(useStyles)((RepoSearch))
