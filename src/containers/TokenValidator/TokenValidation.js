import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

 import './TokenValidation.css'
import * as actions from '../../store/actions/index';


const TokenValidator = (props) => {
    const [tokenState, setToken] = useState('')
    const dispatch = useDispatch();

    const tokenValidator = (token) => dispatch(actions.tokenValidator(token));

    const loading = useSelector(state => state.tokenValidator.loading)
    const error = useSelector(state => state.tokenValidator.error)
    const token = useSelector(state => state.tokenValidator.token)

    const handleForm = (e) => {
        e.preventDefault();
        tokenValidator(tokenState);
    }

    const inputChangeHandler = (e) => {
        setToken(e.target.value);
    }

    let err='';  
    if (error) {
        if (error === 'INVALID') {
            err=(<h4 className="error">Token is InValid.</h4>)
        } else {
            err=(<h4 className="error">{error.message}</h4>);
        }
    }

    return (  
        <div className="TokenPage">
            <div className="Header">
                <h3 className="title">Token Validator</h3>      
                <p>
                    Before Proceeding further please validate your token. 
                    To generate token and find more info refer <a rel="noopener noreferrer" href="https://docs.github.com/en/free-pro-team@latest/graphql/guides" target="_blank"><u>this</u></a>.
                </p>
            </div>

            <form autoComplete="off" onSubmit={handleForm} className="formElement">
                <input type="text" className="textField" name="token" onChange={inputChangeHandler} placeholder="Enter your token here....." />
                {err}
                <button className="btn-inline-form" type="submit">Submit</button>
                    
                    {token ? props.history.push('/repo') : null}
            </form>
        </div>
    )
};

export default TokenValidator;
