import React from 'react'
import { Route, Redirect } from 'react-router'

const PrivateRoute = ({ component: Component, ...rest }) => {
  return (
    <Route {...rest} render={(props) =>
      rest.token ? <Component {...props} /> : <Redirect to="/token" />
    } />
  )
}
export default PrivateRoute;
